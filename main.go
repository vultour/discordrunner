package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/kballard/go-shellquote"
	log "github.com/sirupsen/logrus"
	"golang.org/x/sync/semaphore"
)

// MaxMessageLength determines the maximum length of a single Discord message.
const MaxMessageLength = 1500

var args arguments
var requestID int64
var lock sync.Mutex

func main() {
	args = parseArgs()
	discord, err := discordgo.New(fmt.Sprintf("Bot %s", *args.APIKey))
	if err != nil {
		log.WithField("err", err).Fatal("Could not create bot instance")
	}

	// Add hooks
	discord.AddHandler(messageHandler)

	// Go
	err = discord.Open()
	if err != nil {
		log.WithField("err", err).Fatal("Could not open connection")
	}
	log.Info("Connected")

	// Block until we quit
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sigc

	// Bye
	log.Info("Shutting down")
	discord.Close()
}

func messageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Get request ID
	lock.Lock()
	request := requestID
	requestID++
	lock.Unlock()

	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.HasPrefix(m.Content, "!run ") {
		go handleRun(m.Content[5:], s, m, request)
	}
}

func cumulativeUpdate(c chan string, s *discordgo.Session, m *discordgo.MessageCreate, r int64) {
	log.WithField("id", r).Info("Starting cumulative update thread")
	var err error
	var current strings.Builder
	var currentMessage string // Message.ID
	var finished = false
	var needsUpdate = false
	go func() {
		for !finished {
			time.Sleep(time.Second * 2)
			if needsUpdate {
				needsUpdate = false
				currentMessage, err = sendOrUpdate(currentMessage, current.String(), s, m)
				if err != nil {
					log.WithField("err", err).Error("Could not send message")
					close(c) // TODO: This is really bad and needs fixing
					return
				}
			}
		}
	}()
	for content := range c {
		lines := strings.Split(content, "\n")
		for _, line := range lines {
			line = line + "\n"
			runes := []rune(line)
			if len(runes) > MaxMessageLength {
				runes = runes[:MaxMessageLength-3]
				runes = append(runes, rune('.'), rune('.'), rune('.'))
			}

			if (current.Len() + len(runes)) > MaxMessageLength {
				currentMessage, err = sendOrUpdate(currentMessage, current.String(), s, m)
				if err != nil {
					log.WithField("err", err).Error("Could not send message")
					close(c) // <shrug>
					return
				}
				current.Reset()
				currentMessage = ""
			}
			current.WriteString(string(runes))
			needsUpdate = true
		}
	}
	finished = true
	log.WithField("id", r).Info("Cumulative update thread finished")
}

func sendOrUpdate(current, message string, s *discordgo.Session, m *discordgo.MessageCreate) (string, error) {
	if current == "" {
		message, err := s.ChannelMessageSend(
			m.ChannelID,
			"```"+message+"```",
		)
		if err != nil {
			return current, fmt.Errorf("could not send message: %s", err)
		}
		current = message.ID
	} else {
		_, err := s.ChannelMessageEdit(
			m.ChannelID,
			current,
			"```"+message+"```",
		)
		if err != nil {
			return current, fmt.Errorf("could not update message: %s", err)
		}
	}
	return current, nil
}

func handleRun(c string, s *discordgo.Session, m *discordgo.MessageCreate, r int64) {
	log.WithField("id", r).Info("Processing request")
	parts := strings.Split(strings.TrimSpace(c), " ")
	if len(parts) < 2 {
		log.Error("Command had less than two parts")
		return
	}

	// Create cumulative update channel
	channel := make(chan string)
	defer close(channel)
	go cumulativeUpdate(channel, s, m, r)

	// Clone or pull
	if _, err := os.Stat(GetRepoDir(parts[0], *args.RunDir)); os.IsNotExist(err) {
		log.WithField("repo", parts[0]).Info("Repo does not exist, cloning")
		channel <- "Repository does not exist, cloning:"
		out, err := CloneRepo(parts[0], *args.RunDir)
		if err != nil {
			log.WithFields(log.Fields{
				"err": err,
				"out": string(out),
			}).Error("Git clone failed")
			channel <- fmt.Sprintf("Error: git clone failed: %s", err)
			return
		}
		channel <- out
	} else if err == nil {
		log.WithField("repo", parts[0]).Info("Repo exists, pulling latest")
		channel <- "Repo exists locally, pulling latest version:"
		out, err := UpdateRepo(parts[0], *args.RunDir)
		if err != nil {
			log.WithField("err", err).Error("Git pull failed")
			channel <- fmt.Sprintf("Error: git pull failed: %s", err)
			return
		}
		channel <- out
	} else {
		log.WithField("err", err).Error("Unknown error")
		channel <- fmt.Sprintf("Error: could not stat repo: %s", err)
		return
	}

	// Create cmd
	arg, err := shellquote.Split(strings.Join(parts[1:], " "))
	if err != nil {
		log.WithField("err", err).Error("Could not split command arguments")
		channel <- fmt.Sprintf("Error: could not split arguments: %s", err)
		return
	}
	log.WithField("args", arg).Info("Executing command")

	var cmd *exec.Cmd
	if len(arg) < 2 {
		cmd = exec.Command(arg[0])
	} else {
		cmd = exec.Command(arg[0], arg[1:]...)
	}
	cmd.Dir = GetRepoDir(parts[0], *args.RunDir)

	// Create stdout/stderr readers
	stdouts, err := cmd.StdoutPipe()
	if err != nil {
		log.WithField("err", err).Error("Could not get stdout stream")
		channel <- fmt.Sprintf("Error: could not get stdout stream: %s", err)
		return
	}
	stderrs, err := cmd.StderrPipe()
	if err != nil {
		log.WithField("err", err).Error("Could not get stderr stream")
		channel <- fmt.Sprintf("Error: could not get stderr stream: %s", err)
		return
	}

	// Start the command
	if err := cmd.Start(); err != nil {
		log.WithField("err", err).Error("Could not start command")
		channel <- fmt.Sprintf("Error: could not start command: %s", err)
		return
	}

	// Make sure we don't exit before the Readers are finished
	sem := semaphore.NewWeighted(2)
	sem.Acquire(context.Background(), 2)
	go scanLines(stdouts, channel, sem)
	go scanLines(stderrs, channel, sem)
	sem.Acquire(context.Background(), 2)

	// Wait for the command to exit
	if err := cmd.Wait(); err != nil {
		log.WithField("err", err).Error("Failed waiting for command to finish")
		channel <- fmt.Sprintf("Error: command failed: %s", err)
		return
	}
}

func scanLines(r io.Reader, c chan string, s *semaphore.Weighted) {
	log.Trace("scanLines acquiring lock")
	defer s.Release(1)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		c <- scanner.Text()
	}
	if err := scanner.Err(); err != nil {
		log.WithField("err", err).Error("Scanner returned error")
	}
	log.Trace("scanLines releasing lock")
}
