package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/akamensky/argparse"
	log "github.com/sirupsen/logrus"
)

type arguments struct {
	Parser *argparse.Parser

	Debug *bool
	Trace *bool

	APIKey *string
	RunDir *string

	GitPath *string
}

func parseArgs() arguments {
	a := arguments{}
	a.Parser = argparse.NewParser(
		"discordrunner",
		"Downloads and runs arbitrary code from git",
	)

	a.Debug = a.Parser.Flag("", "debug", &argparse.Options{Help: "Debug log"})
	a.Trace = a.Parser.Flag("", "trace", &argparse.Options{Help: "Trace log"})

	a.APIKey = a.Parser.String(
		"k", "api-key",
		&argparse.Options{
			Required: true,
			Help:     "Discord bot API key, or a path to a file containing it",
		},
	)
	a.RunDir = a.Parser.String(
		"d", "run-dir",
		&argparse.Options{
			Required: true,
			Help:     "Directory where downloaded code will be stored",
		},
	)

	a.GitPath = a.Parser.String(
		"g", "git-path",
		&argparse.Options{
			Help: "Path to the git executable",
		},
	)

	err := a.Parser.Parse(os.Args)
	if err != nil {
		fmt.Print(a.Parser.Usage(err))
		os.Exit(1)
	}

	err = validateArgs(&a)
	if err != nil {
		fmt.Print(a.Parser.Usage(err))
		os.Exit(2)
	}

	return a
}

func validateArgs(a *arguments) error {
	log.SetLevel(log.InfoLevel)
	if *a.Debug {
		log.SetLevel(log.DebugLevel)
	}
	if *a.Trace {
		log.SetLevel(log.TraceLevel)
	}

	if _, err := os.Stat(*a.APIKey); err == nil {
		f, e := os.Open(*a.APIKey)
		if e != nil {
			log.WithField("err", err).Fatal("Could not open API Key file")
		}
		b, e := ioutil.ReadAll(f)
		if e != nil {
			log.WithField("err", err).Fatal("Could not read API Key file")
		}
		*a.APIKey = strings.TrimSpace(string(b))
		log.Info("API key set from file")
	} else {
		log.Info("API key doesn't seem like a file, using raw")
	}

	if *a.GitPath != "" {
		Git = *a.GitPath
	}

	return nil
}
