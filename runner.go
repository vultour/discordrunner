package main

import (
	"fmt"
	"os/exec"
	"path/filepath"
	"strings"
)

// Git points to the git binary
var Git = "/usr/bin/git"

// CloneRepo clones the specified repository into the given folder
func CloneRepo(repo, dir string) (string, error) {
	cmd := exec.Command(
		Git,
		"clone",
		"--progress",
		repo,
		GetRepoDir(repo, dir),
	)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("could not run git clone: %s", err)
	}
	return string(output), err
}

// UpdateRepo pulls the latest version of the repo
func UpdateRepo(repo, dir string) (string, error) {
	cmd := exec.Command(
		Git,
		"pull",
	)
	cmd.Dir = GetRepoDir(repo, dir)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("failed running git pull: %s", err)
	}
	return string(output), nil
}

// GetRepoDir returns a path to the specified repository
func GetRepoDir(repo, dir string) string {
	return filepath.Join(
		dir,
		strings.TrimLeft(strings.TrimLeft(repo, "http://"), "https://"),
	)
}
